class UserPolicy < ApplicationPolicy

  def index?
    user.funcionario?
  end

  class Scope < Scope

    def resolve
      scope.all
    end
  end
end
