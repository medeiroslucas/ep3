class WellcomeController < ApplicationController

  before_action :authenticate_user!

  def index

    @user = current_user

    if params[:search]
      @aux = Cliente.where("Nome like ?", "%#{params[:search]}%")
    else
      @aux = Cliente.all
    end

    if @user.role == 1
      redirect_to admin_path
    end
    
    @aux.each do |item|
      if item.user_id == @user.id
        @cliente = item
        if item.user.role == 1
          
          @tempo = item.veiculo.Saida - item.veiculo.Entrada
        end
      end
    end

    @espaco = Espaco.first

  end

  def sair

    @cliente = Cliente.find(params[:client_id])

    @cliente.veiculo.Estacionado = false

    @cliente.veiculo.Saida = Time.now

    @esta = Espaco.first

    @esta.Vagas = @esta.Vagas + 1

    tempo = (@cliente.veiculo.Saida - @cliente.veiculo.Entrada)/3600

    tempo = tempo.to_i

    @cliente.Horas = @cliente.Horas - tempo

    @esta.save

    @cliente.save

    redirect_to root_path
    
  end
  
  def entrar

    @cliente = Cliente.find(params[:client_id])

    @cliente.veiculo.Estacionado = true

    @cliente.veiculo.Entrada = Time.now

    @esta = Espaco.first

    if @esta.Vagas > 0 && @cliente.Horas > 0
      
      @esta.Vagas = @esta.Vagas - 1
      
      @esta.save
      
      @cliente.save
      
      redirect_to root_path
    
    else
    
      flash[:notice] = "Operação Inválida"

      redirect_to root_path

    end
    
  end

end
