require "application_system_test_case"

class FuncionariosTest < ApplicationSystemTestCase
  setup do
    @funcionario = funcionarios(:one)
  end

  test "visiting the index" do
    visit funcionarios_url
    assert_selector "h1", text: "Funcionarios"
  end

  test "creating a Funcionario" do
    visit funcionarios_url
    click_on "New Funcionario"

    fill_in "Carga Horaria", with: @funcionario.Carga_Horaria
    fill_in "Carros Registrados", with: @funcionario.Carros_Registrados
    fill_in "Cpf", with: @funcionario.Cpf
    fill_in "Idade", with: @funcionario.Idade
    fill_in "Nome", with: @funcionario.Nome
    fill_in "Salario", with: @funcionario.Salario
    fill_in "Sexo", with: @funcionario.Sexo
    fill_in "User", with: @funcionario.user_id
    click_on "Create Funcionario"

    assert_text "Funcionario was successfully created"
    click_on "Back"
  end

  test "updating a Funcionario" do
    visit funcionarios_url
    click_on "Edit", match: :first

    fill_in "Carga Horaria", with: @funcionario.Carga_Horaria
    fill_in "Carros Registrados", with: @funcionario.Carros_Registrados
    fill_in "Cpf", with: @funcionario.Cpf
    fill_in "Idade", with: @funcionario.Idade
    fill_in "Nome", with: @funcionario.Nome
    fill_in "Salario", with: @funcionario.Salario
    fill_in "Sexo", with: @funcionario.Sexo
    fill_in "User", with: @funcionario.user_id
    click_on "Update Funcionario"

    assert_text "Funcionario was successfully updated"
    click_on "Back"
  end

  test "destroying a Funcionario" do
    visit funcionarios_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Funcionario was successfully destroyed"
  end
end
