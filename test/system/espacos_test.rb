require "application_system_test_case"

class EspacosTest < ApplicationSystemTestCase
  setup do
    @espaco = espacos(:one)
  end

  test "visiting the index" do
    visit espacos_url
    assert_selector "h1", text: "Espacos"
  end

  test "creating a Espaco" do
    visit espacos_url
    click_on "New Espaco"

    fill_in "Vagas", with: @espaco.Vagas
    fill_in "Valor Hora", with: @espaco.Valor_Hora
    click_on "Create Espaco"

    assert_text "Espaco was successfully created"
    click_on "Back"
  end

  test "updating a Espaco" do
    visit espacos_url
    click_on "Edit", match: :first

    fill_in "Vagas", with: @espaco.Vagas
    fill_in "Valor Hora", with: @espaco.Valor_Hora
    click_on "Update Espaco"

    assert_text "Espaco was successfully updated"
    click_on "Back"
  end

  test "destroying a Espaco" do
    visit espacos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Espaco was successfully destroyed"
  end
end
