require "application_system_test_case"

class ClientesTest < ApplicationSystemTestCase
  setup do
    @cliente = clientes(:one)
  end

  test "visiting the index" do
    visit clientes_url
    assert_selector "h1", text: "Clientes"
  end

  test "creating a Cliente" do
    visit clientes_url
    click_on "New Cliente"

    fill_in "Cpf", with: @cliente.Cpf
    fill_in "Horas", with: @cliente.Horas
    fill_in "Idade", with: @cliente.Idade
    fill_in "Nome", with: @cliente.Nome
    fill_in "Plano", with: @cliente.Plano
    fill_in "Sexo", with: @cliente.Sexo
    fill_in "User", with: @cliente.user_id
    fill_in "Veiculo", with: @cliente.veiculo_id
    click_on "Create Cliente"

    assert_text "Cliente was successfully created"
    click_on "Back"
  end

  test "updating a Cliente" do
    visit clientes_url
    click_on "Edit", match: :first

    fill_in "Cpf", with: @cliente.Cpf
    fill_in "Horas", with: @cliente.Horas
    fill_in "Idade", with: @cliente.Idade
    fill_in "Nome", with: @cliente.Nome
    fill_in "Plano", with: @cliente.Plano
    fill_in "Sexo", with: @cliente.Sexo
    fill_in "User", with: @cliente.user_id
    fill_in "Veiculo", with: @cliente.veiculo_id
    click_on "Update Cliente"

    assert_text "Cliente was successfully updated"
    click_on "Back"
  end

  test "destroying a Cliente" do
    visit clientes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cliente was successfully destroyed"
  end
end
