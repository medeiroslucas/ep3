require 'test_helper'

class EspacosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @espaco = espacos(:one)
  end

  test "should get index" do
    get espacos_url
    assert_response :success
  end

  test "should get new" do
    get new_espaco_url
    assert_response :success
  end

  test "should create espaco" do
    assert_difference('Espaco.count') do
      post espacos_url, params: { espaco: { Vagas: @espaco.Vagas, Valor_Hora: @espaco.Valor_Hora } }
    end

    assert_redirected_to espaco_url(Espaco.last)
  end

  test "should show espaco" do
    get espaco_url(@espaco)
    assert_response :success
  end

  test "should get edit" do
    get edit_espaco_url(@espaco)
    assert_response :success
  end

  test "should update espaco" do
    patch espaco_url(@espaco), params: { espaco: { Vagas: @espaco.Vagas, Valor_Hora: @espaco.Valor_Hora } }
    assert_redirected_to espaco_url(@espaco)
  end

  test "should destroy espaco" do
    assert_difference('Espaco.count', -1) do
      delete espaco_url(@espaco)
    end

    assert_redirected_to espacos_url
  end
end
