
# ep3

Exercício Programa 3 OO FGA 2018/2
=======
# README

O projeto foi desenvolvido em Rails 5.2.1, usando adicionalmente as gems devise, admin, pundit e bootstrap, para modelagem do banco de dados foi usado o SQLite.


Para executar a aplicação, abra o projeto na IDE de preferência, execute o comando "Rail Server" e entre na Url "http://localhost:3000/" em seu navegador.


O projeto consiste de uma aplicação web de gerência de um Estacionamento voltada para funcionários, no qual o funcionário pode marcar a entrada e saída de carros, podendo também adicionar,
excluir e alterar clientes, enquanto os clientes tem a possibilidade de se cadastrar, excluir sua conta e ver a quantidade de horas disponíveis

