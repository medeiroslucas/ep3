class CreateFuncionarios < ActiveRecord::Migration[5.2]
  def change
    create_table :funcionarios do |t|
      t.string :Nome
      t.integer :Idade
      t.string :Cpf
      t.string :Sexo
      t.integer :Carros_Registrados
      t.float :Salario
      t.integer :Carga_Horaria
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
