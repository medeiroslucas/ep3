class CreateClientes < ActiveRecord::Migration[5.2]
  def change
    create_table :clientes do |t|
      t.string :Nome
      t.integer :Idade
      t.string :Cpf
      t.string :Sexo
      t.string :Plano
      t.integer :Horas
      t.references :veiculo, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
