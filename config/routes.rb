Rails.application.routes.draw do
  resources :espacos
  resources :funcionarios
  resources :clientes
  resources :veiculos
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  resources :veiculos
  get 'home/index'
  get 'wellcome/index'
  get 'wellcome/sair'
  get 'wellcome/entrar'
  devise_for :users, controllers: { registrations: "registrations" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  authenticated :user do
    root 'wellcome#index', as: :authenticated_root
  end


  root 'home#index'
  #root '/users/sign_in'


end
